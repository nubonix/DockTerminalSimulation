# Helpful tip
When viewing the .png [ pictures ], hold the left shift and use the mouse scroll wheel to move the picture horizontally

# Contact information:
`Discord:` `nubonix#3648`

# DockTerminalSimulation
An attempt at creating a simulated dock terminal environment for machine learning. E.g. train an ai agent to drive a forklift, move freight in and out of trailers, train another agent to move trailers from the yard to the dock terminal, from the dock terminal to the yard, and from the dock terminal to another location of the dock terminal.
